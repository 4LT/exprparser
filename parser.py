from string import ascii_letters

class Dfa:
    def __init__(self, states):
        self._states = states
        self._cur_idx = 0
        self._prev_accepts = False

    def transition(self, sym):
        if self.dead():
            self._prev_accepts = False
        else:
            self._prev_accepts = self._states[self._cur_idx].accepts
            if sym in self._states[self._cur_idx].transitions:
                self._cur_idx = self._states[self._cur_idx].transitions[sym]
            else:
                self._cur_idx = None

    def reset(self):
        self._cur_idx = 0
        self._prev_accepts = False

    def dead(self):
        return self._cur_idx is None

    def prev_accepts(self):
        return self._prev_accepts

    def __repr__(self):
        return "<cur: %s, prev accepts: %s, states: %s>" %\
                (self._cur_idx, self._prev_accepts, self._states)

class DfaState:
    def __init__(self, accepts, transitions):
       self.accepts = accepts
       self.transitions = transitions

    def __repr__(self):
        return "DfaState(" + repr(self.accepts) + ", " +\
                repr(self.transitions) + ")"

    def __str__(self):
        return repr(self)

class Token:
    def __init__(self, tok_type, value):
        self.type = tok_type
        self.value = value

    def __repr__(self):
        return "Token(%s, %s)" % (repr(self.type), repr(self.value))

    def __str__(self):
        return "%s(%s)" % (self.type, repr(self.value))

class AstNode:
    def __init__(self, node_type, *values):
        self.type = node_type
        self.values = list(values)

    def __repr__(self):
        return "AstNode(%s)" % ", ".join(\
                [ repr(v) for v in [self.type] + self.values])

    def __str__(self):
        return "%s(%s)" % (self.type, ", ".join(\
                [(str(v) if isinstance(v, AstNode) else repr(v))\
                for v in self.values]))

class TokenError(Exception):
    def __init__(self, message):
        super().__init__(message)

class ExprSyntaxError(Exception):
    def __init__(self, message=None):
        if message is None:
            super().__init__("Syntax Error.")
        else:
            super().__init__("Syntax Error. " + message)

class EvaluationError(Exception):
    def __init__(self, message):
        super().__init__(message)

def poly_transition(syms, state):
    return {sym: state for sym in syms}

def digit_transition(state):
    return {str(ch): state for ch in range(10)}

def sequence_dfa(seq):
    states = []
    for i in range(len(seq)):
        states.append(DfaState(False, {seq[i]: i+1}))
    states.append(DfaState(True, {}))
    return Dfa(states)

def set_dfa(sym_set):
    states = [\
        DfaState(False, {**poly_transition(sym_set, 1)}),
        DfaState(True, {**poly_transition(sym_set, 1)})]
    return Dfa(states)

def number_dfa():
    states = [\
        DfaState(False, {**digit_transition(1), '.': 2}),
        DfaState(True, {**digit_transition(1), **poly_transition('eE', 4),
            '.': 3}),
        DfaState(False, {**digit_transition(3)}),
        DfaState(True, {**digit_transition(3), **poly_transition('eE', 4)}),
        DfaState(False, {**digit_transition(5), **poly_transition('+-', 6)}),
        DfaState(True, {**digit_transition(5)}),
        DfaState(False, {**digit_transition(5)}) ]
    return Dfa(states)

def var_dfa():
    states = [\
        DfaState(False, {'$': 1}),
        DfaState(True, {**poly_transition(ascii_letters + '_', 1),
            **digit_transition(1)}) ]
    return Dfa(states)

def lex(text):
    ch_idx = 0
    token_start = 1
    token_value = ""
    tokens = []
    dfas = [\
        (sequence_dfa("("), "OP"),
        (sequence_dfa(")"), "CP"),
        (sequence_dfa("+"), "PLUS"),
        (sequence_dfa("-"), "MINUS"),
        (sequence_dfa("*"), "STAR"),
        (sequence_dfa("/"), "SLASH"),
        (number_dfa(), "NUM"),
        (var_dfa(), "VAR"),
        (set_dfa("\n\r\t\v\0 "), None)]

    def all_dfas_dead():
        for dfa, _ in dfas:
            if not dfa.dead():
                return False
        return True

    def step_dfas(ch):
        for dfa, _ in dfas:
            dfa.transition(ch)

    def reset_dfas():
        for dfa, _ in dfas:
            dfa.reset()

    def append_token():
        tok = None
        ignore = False

        for dfa, tok_type in dfas:
            if dfa.prev_accepts():
                if tok_type is None:
                    ignore = True
                    continue
                tok = Token(tok_type, token_value)
                break

        if tok is None and ignore:
            pass
        elif tok is None:
            raise TokenError('Could not match token at column %s.'\
                    % token_start)
        else:
            tokens.append(tok)

    for ch in text:
        step_dfas(ch)
        ch_idx+= 1
        if (all_dfas_dead()):
            append_token()
            reset_dfas()
            token_start = ch_idx
            token_value = ""
            step_dfas(ch)
        token_value+= ch
    step_dfas(None)
    append_token()

    return tokens

def parse(tokens):
    head = None
    rest = tokens[:]

    def consume():
        nonlocal head
        nonlocal rest

        if len(rest) < 1:
            return False
        else:
            head = rest[0]
            rest = rest[1:]
            return True

    def unconsume():
        nonlocal head
        nonlocal rest

        if head is None:
            raise RuntimeError("Cannot backtrack further")
        else:
            rest = [head] + rest
            head = None

    def parse_start():
        ast = parse_expression()
        if consume():
            raise ExprSyntaxError("Excess input.")
        else:
            return ast

    def parse_binary(ops, next_rule):
        left = next_rule()
        done = False
        while not done:
            if consume():
                if head.type in ops:
                    op = ops[head.type]
                    left = AstNode(op, left, next_rule())
                else:
                    unconsume()
                    done = True
            else:
                done = True

        return left

    def parse_expression():
        return parse_binary({'PLUS':'ADD', 'MINUS':'SUB'}, parse_term)

    def parse_term():
        return parse_binary({'STAR':'MUL', 'SLASH':'DIV'}, parse_factor)

    def parse_factor():
        if consume():
            if head.type == 'OP':
                sub_ast = parse_expression()
                if consume():
                    if head.type == 'CP':
                        return sub_ast
                    else:
                        raise ExprSyntaxError("Expected closing parenthesis.")
                else:
                    raise ExprSyntaxError()
            elif head.type == 'MINUS':
                return AstNode('NEG', parse_factor())
            elif head.type == 'PLUS':
                return parse_factor()
            elif head.type == 'VAR':
                return AstNode('VAR', head.value)
            elif head.type == 'NUM':
                return AstNode('NUM', head.value)
            else:
                raise ExprSyntaxError()
        else:
            raise ExprSyntaxError()

    return parse_start()

def evaluate(ast, env):
    env = dict(env)

    def my_div(x, y):
        x = evaluate(x, env)[0]
        y = evaluate(y, env)[0]
        if y == 0.0:
            raise EvaluationError("Divide by zero.")
        else:
           return x / y

    def read_env(key):
        if key in env:
            return float(env[key])
        else:
            raise EvaluationError("Unbound variable %s." % key)

    funcs = {\
            "ADD": lambda x, y:\
                evaluate(x, env)[0] + evaluate(y, env)[0],
            "SUB": lambda x, y:\
                evaluate(x, env)[0] - evaluate(y, env)[0],
            "MUL": lambda x, y:\
                evaluate(x, env)[0] * evaluate(y, env)[0],
            "DIV": my_div,
            "NEG": lambda x: -evaluate(x, env)[0],
            "NUM": lambda x: float(x),
            "VAR": read_env }
    result = funcs[ast.type](*ast.values)
    env['$last'] = result
    return (result, env)

def repl():
    env = {}
    while True:
        print(">", end='')
        try:
            in_text = input()
        except KeyboardInterrupt:
            break

        if in_text == "":
            break
        try:
            result, env = evaluate(parse(lex(in_text)), env)
        except (TokenError, ExprSyntaxError, EvaluationError) as err:
            print(str(err))
        else:
            print(result)

if __name__ == "__main__":
    repl()
